const { spawn, exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const { PassThrough } = require('stream');
const os = require('os');

const merge = (...streams) => {
  let pass = new PassThrough();
  let waiting = streams.length;
  for (const stream of streams) {
    pass = stream.pipe(pass, { end: false });
    stream.once('end', () => --waiting === 0 && pass.emit('end'));
  }
  return pass;
};

const target = 'https://graduapp-dev-assets.s3.amazonaws.com/1593558519903-IMG_0020.MP4';
const output = path.join(__dirname, 'gradvid-thumbnail.jpg');

const ffmpegPath = '/usr/local/bin/ffmpeg';
// const args = [
//   '-y',
//   '-i',
//   target,
//   '-vf',
//   `thumbnail,scale=200:200`,
// ]

// function ffmpegDuplexExecute (path, args) {
//   const ffmpeg = spawn(path, args, { shell: true })

//   console.log("ffmpegDuplexExecute args HOY: " + args)

//   return duplexify(ffmpeg.stdin, ffmpeg.stdout)
// }

const ffmpeg = spawn(
  ffmpegPath,
  [
    '-ss',
    1,
    '-y',
    '-i',
    target,
    '-vf',
    `scale=w=320:h=320:force_original_aspect_ratio=increase`,
    '-qscale:v',
    '2',
    '-frames:v',
    '1',
    '-f',
    'image2',
    '-c:v',
    'mjpeg',
    'pipe:1',
    // output
  ],
  { shell: true }
);

console.log('os.tmpDir()', os.tmpdir());

// const rstream = fs.createReadStream(target, { start: 1000, end: 10000 })
// // const streamable = ffmpegDuplexExecute(ffmpegPath, args)

// rstream.pipe(ffmpeg.stdin)

const wstream = fs.createWriteStream(output);

ffmpeg.stdout.pipe(wstream);

ffmpeg.on('close', function (code) {
  console.log('Close');
  wstream.end();
  // resolve()
});

ffmpeg.on('error', function (err) {
  console.log(err);
  // reject()
});

// streamable.pipe(wstream)

// streamable.pipe(wstream)

// var readerStream = fs.createReadStream('gradpic5.jpg');
// var writerStream = fs.createWriteStream('gradpic5-thumbnail.jpg');

// // pipe the read and write operations
// // read input file and write data to output file
// readerStream.pipe(writerStream);

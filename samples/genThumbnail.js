const { spawn, exec } = require('child_process');
const duplexify = require('duplexify');
const path = require('path');
const fs = require('fs');
const { PassThrough } = require('stream');

const merge = (...streams) => {
  let pass = new PassThrough();
  let waiting = streams.length;
  for (const stream of streams) {
    pass = stream.pipe(pass, { end: false });
    stream.once('end', () => --waiting === 0 && pass.emit('end'));
  }
  return pass;
};

const target = path.join(__dirname, 'gradpic5.jpg');
const output = path.join(__dirname, 'gradpic5-thumbnail.jpg');

const ffmpegPath = path.join(__dirname, '../node_modules/ffmpeg');
const args = [
  '-y',
  '-i',
  'pipe:0',
  '-vf',
  `scale=w=320:h=320:force_original_aspect_ratio=increase`,
  '-qscale:v',
  '2',
  '-frames:v',
  '1',
  '-f',
  'image2',
  '-c:v',
  'mjpeg',
  'pipe:1',
  // output
];

function ffmpegDuplexExecute(path, args) {
  const ffmpeg = spawn(path, args, { shell: true });

  console.log(`ffmpegDuplexExecute args HOY: ${args}`);

  return duplexify(ffmpeg.stdin, ffmpeg.stdout);
}

// const ffmpeg = spawn(ffmpegPath, args, { shell: true })

const rstream = fs.createReadStream(target);
const streamable = ffmpegDuplexExecute(ffmpegPath, args);
const wstream = fs.createWriteStream(output);

rstream.pipe(streamable).pipe(wstream);

streamable.on('close', function (code) {
  console.log('Close');
  wstream.end();
  // resolve()
});

streamable.on('error', function (err) {
  console.log(err);
  // reject()
});

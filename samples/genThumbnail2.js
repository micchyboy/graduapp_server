const { spawn, exec } = require('child_process');
const pathToFfmpeg = require('ffmpeg-static');
const path = require('path');
const fs = require('fs');
const { PassThrough } = require('stream');

const merge = (...streams) => {
  let pass = new PassThrough();
  let waiting = streams.length;
  for (const stream of streams) {
    pass = stream.pipe(pass, { end: false });
    stream.once('end', () => --waiting === 0 && pass.emit('end'));
  }
  return pass;
};

const target = path.join(__dirname, 'gradpic5.jpg');
const output = path.join(__dirname, 'gradpic5-thumbnail.jpg');

const ffmpegPath = pathToFfmpeg;
// const args = [
//   '-y',
//   '-i',
//   target,
//   '-vf',
//   `thumbnail,scale=200:200`,
// ]

// function ffmpegDuplexExecute (path, args) {
//   const ffmpeg = spawn(path, args, { shell: true })

//   console.log("ffmpegDuplexExecute args HOY: " + args)

//   return duplexify(ffmpeg.stdin, ffmpeg.stdout)
// }

const ffmpeg = spawn(
  ffmpegPath,
  [
    '-y',
    '-i',
    'pipe:0',
    '-vf',
    `scale=250:250`,
    '-qscale:v',
    '2',
    '-frames:v',
    '1',
    '-f',
    'image2',
    '-c:v',
    'mjpeg',
    'pipe:1',
    // output
  ],
  { shell: true }
);

const rstream = fs.createReadStream(target);
// const streamable = ffmpegDuplexExecute(ffmpegPath, args)

rstream.pipe(ffmpeg.stdin);

const wstream = fs.createWriteStream(output);

ffmpeg.stdout.pipe(wstream);

ffmpeg.on('close', function (code) {
  console.log('Close');
  wstream.end();
  // resolve()
});

ffmpeg.on('error', function (err) {
  console.log(err);
  // reject()
});

// streamable.pipe(wstream)

// streamable.pipe(wstream)

// var readerStream = fs.createReadStream('gradpic5.jpg');
// var writerStream = fs.createWriteStream('gradpic5-thumbnail.jpg');

// // pipe the read and write operations
// // read input file and write data to output file
// readerStream.pipe(writerStream);

const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3-transform');
const genThumbnail = require('simple-thumbnail');
const stream = require('stream');
const path = require('path');
const { Transform, PassThrough } = require('stream');
const config = require('../config/config');

const { ffmpegPath } = config;

const merge = (...streams) => {
  let pass = new PassThrough();
  let waiting = streams.length;
  for (const stream of streams) {
    pass = stream.pipe(pass, { end: false });
    stream.once('end', () => --waiting === 0 && pass.emit('end'));
  }
  return pass;
};

const s3 = new AWS.S3({
  accessKeyId: config.aws.accessKey,
  secretAccessKey: config.aws.secretAccessKey,
});

const uploadS3 = multer({
  storage: multerS3({
    s3,
    acl: 'public-read',
    bucket: 'graduapp-dev-assets',
    contentType: multerS3.AUTO_CONTENT_TYPE,

    shouldTransform(req, file, cb) {
      console.log('in should transform ', file);
      // cb(null, /^image/i.test(file.mimetype))
      cb(null, true);
    },
    transforms: [
      {
        id: 'original',
        key(req, file, cb) {
          console.log(`original: ${file.originalname}`, file);
          cb(null, `${Date.now().toString()}-original-${file.originalname}`);
        },
        transform(req, file, cb) {
          const pass = new PassThrough();
          cb(null, pass);
        },
      },
      {
        id: 'thumbnail',
        key(req, file, cb) {
          console.log(`thumbnail: ${file.originalname}`, file);
          cb(null, `${Date.now().toString()}-${file.originalname}`);
        },
        transform(req, file, cb) {
          console.log('original1: ', file);

          let args = [
            '-i',
            'pipe:0',
            '-vf',
            `scale=250:250`,
            '-qscale:v',
            '2',
            '-frames:v',
            '1',
            '-f',
            'image2',
            '-c:v',
            'mjpeg',
            'pipe:1',
            // output
          ];

          if (file.mimetype === 'video/mp4') {
            args = ['-ss', 1].concat(args);
          }

          console.log('args: ', args);
          const streamable = genThumbnail(null, null, null, {
            path: ffmpegPath,
            args,
          });

          streamable.on('close', function (code) {
            console.log('Close');
            // resolve()
          });

          streamable.on('error', function (err) {
            console.log(err);
            // reject()
          });

          const pass = new PassThrough();

          cb(null, file.mimetype === 'video/mp4' ? pass : streamable);
        },
      },
    ],
  }),
});

class StorageStream extends stream.Duplex {
  constructor() {
    super({
      objectMode: true,
    });

    this.chunks = [];
    this.size = 0;
  }

  _write(chunk, encoding, next) {
    this.chunks.push(chunk);
    console.log('this.chunks write', this.chunks.length);

    next();
  }

  _read(size) {
    console.log('read', size, this.size);
    if (size !== this.size) {
      this.size = size;
      console.log('this.chunks read:', size, this.chunks.length);
      this.push(Buffer.concat(this.chunks));
    } else {
      console.log('END', size, this.chunks.length);
      this.push(null); // 'end'
    }
  }
}

module.exports = uploadS3;

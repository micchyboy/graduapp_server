var AWS = require('aws-sdk');

AWS.config.update({accessKeyId: config.AWSAccessKeyId, secretAccessKey: config.AWSSecretKey,{region: config.AWSRegion}});

var sns = AWS.SNS();

var params ={
	Name:"test",
	Platform:"GCM",//or APNS (Apple ) or ADM (Amazon)
	Attributes:{ // required
		PlatformCredential:config.PlatformCredential // API key which is used to register your app in play store
		//http://docs.aws.amazon.com/sns/latest/api/API_SetPlatformApplicationAttributes.html => for more info about Attributes
	}
};

/*
    Create PlatformApplication Where you can register users mobile endpoints .
*/
sns.createPlatformApplication(params,function (err,data) {
	if(err){
		console.log(err);
	}
	else{
		console.log(data); // retrive PlatformApplicationArn
	}
});

params ={
    PlatformApplicationArn= "string_value" // your PlatformApplication ARN    STRING
    Token : "string_value" // every phone is given token when phone download and register to your app.   STRING
    CustomUserData :"String_value" // you can write any thing about user so that you can identify him
    Attributes:{
        //optional
        //http://docs.aws.amazon.com/sns/latest/api/API_SetEndpointAttributes.html => for more info
    }
}

/*
    It will register mobile to platform application so that 
    if we send notification to platform application it 
    will send notifications to all mobile endpoints registered
*/
sns.createPlatformEndpoint(params,function(err,data){
    if(err){
        console.log(err);
    }
    else{
        console.log(data);
    }
});

//creates topics where users can subscribe to the topics

params ={
    Name:"string_value"
}
sns.createTopic(params,function(err,data){
    if(err){
        console.log(err);
    }
    else{
        console.log(data); // returns topic ARn
    }
});

// publish to particular topic ARN or to endpoint ARN
params = {
    Message: 'STRING_VALUE', /* required */
     MessageAttributes: {
        someKey: {
            DataType: 'STRING_VALUE', /* required */
            BinaryValue: new Buffer('...') || 'STRING_VALUE',
            StringValue: 'STRING_VALUE'
        },
    },
    MessageStructure: 'STRING_VALUE',
    Subject: 'STRING_VALUE',
    TargetArn: 'STRING_VALUE',
    TopicArn: 'STRING_VALUE'
};
sns.publish(params, function(err, data) {
    if (err) {
        console.log(err, err.stack); 
    }
    else{
    }    console.log(data);
});


//subscribe to particular topic
params = {
    Protocol: 'STRING_VALUE', /* required */   //http , https ,application
    TopicArn: 'STRING_VALUE', /* required */   // topic you want to subscribe
    Endpoint: 'STRING_VALUE' // the endpoint that you want to receive notifications.
};
sns.subscribe(params, function(err, data) {
    if (err){ 
        console.log(err);
    }
    else{    
        console.log(data);
    }
});
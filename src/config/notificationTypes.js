const notificationTypes = ['public', 'school', 'tagged'];

module.exports = {
  notificationTypes,
};

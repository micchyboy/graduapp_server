const Const = require('../constants');

const roles = [Const.ROLES.STUDENT, Const.ROLES.PARENT, Const.ROLES.SCHOOL_ADMIN];

const roleRights = new Map();
roleRights.set(Const.ROLES.STUDENT, []);
roleRights.set(Const.ROLES.PARENT, []);
roleRights.set(Const.ROLES.SCHOOL_ADMIN, ['getUsers', 'manageUsers']);

module.exports = {
  roles,
  roleRights,
};

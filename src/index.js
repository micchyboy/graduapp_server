const mongoose = require('mongoose');
const http = require('http');
// const io = require('socket.io');
const app = require('./app');

const httpServer = http.Server(app);
// const ioServer = io(httpServer);
const config = require('./config/config');
const logger = require('./config/logger');

// const { feedService } = require('./services');

let server;
mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  logger.info('Connected to MongoDB');

  // ioServer.on('connection', async (socket) => {
  //   // console.log('A user connected');

  //   const feeds = await feedService.getFeeds();

  //   socket.emit('feeds', feeds);
  //   // Whenever someone disconnects this piece of code executed
  //   socket.on('disconnect', function () {
  //     // console.log('A user disconnected');
  //   });
  // });

  server = httpServer.listen(config.port, () => {
    logger.info(`Listening to port ${config.port}`);
  });
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});

module.exports.activityValidation = require('./activity.validation');
module.exports.schoolValidation = require('./school.validation');
module.exports.feedValidation = require('./feed.validation');
module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./user.validation');
module.exports.notificationValidation = require('./notification.validation');

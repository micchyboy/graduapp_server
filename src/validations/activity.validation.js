const Joi = require('@hapi/joi');

const getActivities = {
  query: Joi.object().keys({
    isVerified: Joi.string(),
    school: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const createSuggestion = {
  body: Joi.object().keys({
    name: Joi.string().required(),
  }),
};

module.exports = {
  getActivities,
  createSuggestion,
};

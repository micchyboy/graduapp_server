const Joi = require('@hapi/joi');
const { password } = require('./custom.validation');
const Const = require('../constants');

const studentRegister = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    parentEmail: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    role: Joi.string().required(),
    yearOfGraduation: Joi.number().required(),
    school: Joi.string().required(),
    extraCurricularInSchool: Joi.array().items(Joi.string()),
    extraCurricularOutSchool: Joi.array().items(Joi.string()),
  }),
};

const parentRegister = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    role: Joi.string().required(),
  }),
};

const adminRegister = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    role: Joi.string().required(),
    school: Joi.string().required(),
    address: Joi.string().required(),
    activities: Joi.array().items(Joi.string()),
  }),
};

const emailVerification = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
};

const login = {
  body: Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
  }),
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
  }),
};

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

module.exports = {
  [`${Const.ROLES.STUDENT}Register`]: studentRegister,
  [`${Const.ROLES.PARENT}Register`]: parentRegister,
  [`${Const.ROLES.SCHOOL_ADMIN}Register`]: adminRegister,
  emailVerification,
  login,
  refreshTokens,
  forgotPassword,
  resetPassword,
};

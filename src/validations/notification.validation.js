const Joi = require('@hapi/joi');

const getNotifications = {
  query: Joi.object().keys({
    user: Joi.string().required(),
    type: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    // page: Joi.number().integer(),
  }),
};

const markNotifsAsSeen = {
  body: Joi.object().keys({
    user: Joi.string().required(),
    notifications: Joi.array().items(Joi.string()).required(),
  }),
};

const storeDeviceToken = {
  body: Joi.object().keys({
    deviceId: Joi.string(),
    isIos: Joi.boolean().required(),
    token: Joi.string().required(),
    user: Joi.string().required(),
    schoolId: Joi.string().required(),
  }),
};

const changeSubscriptionStatus = {
  body: Joi.object().keys({
    user: Joi.string().required(),
    type: Joi.string().required(),
    status: Joi.boolean().required(),
    isIos: Joi.boolean().required(),
  }),
};

module.exports = {
  getNotifications,
  markNotifsAsSeen,
  storeDeviceToken,
  changeSubscriptionStatus,
};

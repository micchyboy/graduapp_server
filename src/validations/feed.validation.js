const Joi = require('@hapi/joi');

const createFeed = {
  body: Joi.object().keys({
    user: Joi.string().required(),
    description: Joi.string().required(),
    type: Joi.string().required(),
    taggedUsers: Joi.array(),
    categories: Joi.any().required(),
    captions: Joi.array().items(Joi.string().empty('').default('')),
  }),
};

const getFeeds = {
  query: Joi.object().keys({
    user: Joi.string().required(),
    type: Joi.string(),
    categories: Joi.string().empty('').default(''),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

module.exports = {
  createFeed,
  getFeeds,
};

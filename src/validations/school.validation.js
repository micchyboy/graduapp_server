const Joi = require('@hapi/joi');
const { objectId } = require('./custom.validation');

const getSchools = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const inviteSchools = {
  body: Joi.object().keys({
    schools: Joi.array().items({
      email: Joi.string().required().email(),
      adminName: Joi.string().required(),
      position: Joi.string(),
      name: Joi.string().required(),
    }),
    invitedBy: Joi.string().required(),
  }),
};

const updateSchool = {
  params: Joi.object().keys({
    schoolId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      address: Joi.string(),
      activities: Joi.array(),
    })
    .min(1),
};

module.exports = {
  getSchools,
  inviteSchools,
  updateSchool,
};

module.exports.Activity = require('./activity.model');
module.exports.School = require('./school.model');
module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Feed = require('./feed.model');
module.exports.Notification = require('./notification.model');
module.exports.UserNotification = require('./userNotification.model');

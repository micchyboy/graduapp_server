const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const schoolSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
    address: {
      type: String,
      required: true,
      trim: true,
    },
    activities: {
      type: [
        {
          activity: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Tblt_activity',
          },
          isVerified: {
            type: Boolean,
            default: false,
          },
        },
      ],
      default: [],
    },
    // admins: {
    //   type: [
    //     {
    //       type: mongoose.SchemaTypes.ObjectId,
    //       ref: 'Tblm_user',
    //     },
    //   ],
    // },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
  }
);

// add plugin that converts mongoose to json
schoolSchema.plugin(toJSON);
schoolSchema.plugin(paginate);

/**
 * Check if school exists
 * @param {string} name - The school's name
 * @param {ObjectId} [excludeSchoolId] - The id of the school to be excluded
 * @returns {Promise<boolean>}
 */
schoolSchema.statics.isSchoolTaken = async function (name, excludeSchoolId) {
  const school = await this.findOne({ name, _id: { $ne: excludeSchoolId } });
  return !!school;
};

/**
 * @typedef School
 */
const School = mongoose.model('Tblm_school', schoolSchema);

module.exports = School;

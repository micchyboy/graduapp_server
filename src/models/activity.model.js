const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const activitySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
activitySchema.plugin(toJSON);
activitySchema.plugin(paginate);

/**
 * Check if activity exists
 * @param {string} name - The activity's name
 * @param {ObjectId} [excludeActivityId] - The id of the activity to be excluded
 * @returns {Promise<boolean>}
 */
activitySchema.statics.isActivityTaken = async function (name, excludeActivityId) {
  const activity = await this.findOne({ name, _id: { $ne: excludeActivityId } });
  return !!activity;
};

/**
 * @typedef Activity
 */
const Activity = mongoose.model('Tblt_activity', activitySchema);

module.exports = Activity;

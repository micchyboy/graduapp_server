const mongoose = require('mongoose');
const { toJSON } = require('./plugins');
const { notificationTypes } = require('../config/notificationTypes');

const notificationSchema = mongoose.Schema(
  {
    type: {
      type: String,
      enum: notificationTypes,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Tblm_user',
      required: true,
    },
    taggedUsers: {
      type: [
        {
          type: mongoose.SchemaTypes.ObjectId,
          ref: 'Tblm_user',
        },
      ],
      default: undefined,
    },
    school: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Tblm_school',
      required: true,
    },
    feed: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Tblt_feed',
      required: true,
    },
    isSubscribedToPublic: {
      type: Boolean,
      default: true,
    },
    isSubscribedToSchool: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
notificationSchema.plugin(toJSON);

/**
 * @typedef Notification
 */
const Notification = mongoose.model('Tblt_notification', notificationSchema);

module.exports = Notification;

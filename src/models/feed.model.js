const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const { feedTypes } = require('../config/feedTypes');

const feedSchema = mongoose.Schema(
  {
    description: {
      type: String,
      required: true,
      trim: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Tblm_user',
      required: true,
    },
    type: {
      type: String,
      enum: feedTypes,
      default: 'public',
    },
    categories: {
      type: [
        {
          name: {
            type: String,
            required: true,
            trim: true,
          },
          subCategories: {
            type: [
              {
                type: String,
                required: true,
                trim: true,
              },
            ],
            trim: true,
          },
        },
      ],
      default: [],
    },
    school: {
      type: String,
      required: true,
      trim: true,
    },
    files: [
      {
        type: {
          type: String,
          required: true,
          trim: true,
        },
        thumbnail: {
          type: String,
          required: true,
          trim: true,
        },
        url: {
          type: String,
          required: true,
          trim: true,
        },
        caption: {
          type: String,
          trim: true,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
feedSchema.plugin(toJSON);
feedSchema.plugin(paginate);

/**
 * @typedef Feed
 */
const Feed = mongoose.model('Tblt_feed', feedSchema);

module.exports = Feed;

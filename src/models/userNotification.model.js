const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const userNotificationSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Tblm_user',
      required: true,
      unique: true,
    },
    deviceTokens: {
      type: mongoose.SchemaTypes.Mixed,
    },
    endpoints: {
      type: mongoose.SchemaTypes.Mixed,
    },
    notifications: {
      type: [
        {
          type: mongoose.SchemaTypes.ObjectId,
          ref: 'Tblt_notification',
        },
      ],
      default: [],
    },
    school: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Tblm_school',
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userNotificationSchema.plugin(toJSON);
// userNotificationSchema.plugin(paginate);

/**
 * @typedef UserNotification
 */
const UserNotification = mongoose.model('Tblmq_user_notification', userNotificationSchema);

module.exports = UserNotification;

const AWS = require('aws-sdk');
const { spawn } = require('child_process');
const pathToFfmpeg = require('ffmpeg-static');

AWS.config.update({
  accessKeyId: 'AKIAXJHBNQN2FXBF335S',
  secretAccessKey: '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',
});

const s3 = new AWS.S3();

const ffmpegPath = pathToFfmpeg;

const generateThumbnail = (target, mimetype) => {
  let args = [
    '-y',
    '-i',
    target,
    '-vf',
    `scale=w=320:h=320:force_original_aspect_ratio=increase`,
    '-qscale:v',
    '2',
    '-frames:v',
    '1',
    '-f',
    'image2',
    '-c:v',
    'mjpeg',
    'pipe:1',
    // output
  ];

  if (mimetype === 'video/mp4') {
    args = ['-ss', 1].concat(args);
  }

  const ffmpeg = spawn(ffmpegPath, args, { shell: true });

  return ffmpeg;
};

const uploadToS3 = (output, key, cb) => {
  // Setting up S3 upload parameters
  const params = {
    Bucket: 'graduapp-dev-assets',
    Key: key, // File name you want to save as in S3
    Body: output,
    ACL: 'public-read',
  };

  // Uploading files to the bucket
  return s3.upload(params, cb);
};

module.exports = (originalUrl, thumbnailUrl, mimetype) => {
  return new Promise((resolve, reject) => {
    const route = thumbnailUrl.replace(/.*?:\/\//g, '');
    const key = route.split('/')[1];

    const fsStream = generateThumbnail(originalUrl, mimetype);

    uploadToS3(fsStream.stdout, key, function (err, data) {
      if (err) {
        reject(err);
      }

      resolve(data.Location);
    });

    fsStream.on('close', function () {
      // console.log("CLOSE FSTREAM")
    });

    fsStream.on('error', function (err) {
      reject(err);
    });
  });
};

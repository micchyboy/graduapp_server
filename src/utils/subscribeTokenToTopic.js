// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
const logger = require('../config/logger');
const { getPlatformApplicationArn } = require('./awsUtils');
// Set region
AWS.config.update({
  accessKeyId: 'AKIAXJHBNQN2FXBF335S',
  secretAccessKey: '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',
  region: 'us-east-1',
});

const sns = new AWS.SNS();

module.exports = async (token, topicName, isIos) => {
  logger.info(`subscribeTokenToTopic device platform: ${isIos ? 'iOS' : 'android'}`);
  logger.info(`subscribeTokenToTopic token: ${token} topicName: ${topicName}`);
  const platformApplicationArn = getPlatformApplicationArn(isIos);
  const endpointParams = { PlatformApplicationArn: platformApplicationArn, Token: token };
  const topicParams = {
    Name: topicName,
  };
  return new Promise((resolve, reject) => {
    sns.createPlatformEndpoint(endpointParams, function (errEndpoint, EndPointResult) {
      if (errEndpoint) {
        logger.error(`Error on createPlatformEndpoint: ${errEndpoint}`);
        reject(errEndpoint);
      } else {
        const { EndpointArn } = EndPointResult;
        logger.info(`EndpointArn: ${EndpointArn}`);

        sns.createTopic(topicParams, function (errTopic, dataTopic) {
          if (errTopic) {
            logger.error(`Error on createTopic: ${errTopic}`);
            reject(errTopic);
          } else {
            const { TopicArn } = dataTopic;

            logger.info(`TopicArn: ${TopicArn}`);

            const subscribeParams = {
              Protocol: 'application' /* required */,
              TopicArn /* required */,
              Endpoint: EndpointArn,
              // ReturnSubscriptionArn: true || false
            };
            sns.subscribe(subscribeParams, function (errSub, dataSub) {
              if (errSub) {
                logger.error(`Error on subscribe to topic: ${errSub}`);
                reject(errSub);
              } else {
                const { SubscriptionArn } = dataSub;
                resolve({ SubscriptionArn, EndpointArn });
              }
            });
          }
        });
      }
    });
  });
};

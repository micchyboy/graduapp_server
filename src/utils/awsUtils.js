const getPlatformApplicationArn = (isIos) => {
  const notifService = isIos ? 'APNS_SANDBOX' : 'GCM';
  return `arn:aws:sns:us-east-1:500836434804:app/${notifService}/graduapp`;
};

module.exports = {
  getPlatformApplicationArn,
};

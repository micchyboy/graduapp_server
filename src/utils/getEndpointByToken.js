const AWS = require('aws-sdk');
const { getPlatformApplicationArn } = require('./awsUtils');
const logger = require('../config/logger');

// Load the AWS SDK for Node.js
// Set region
AWS.config.update({
  accessKeyId: 'AKIAXJHBNQN2FXBF335S',
  secretAccessKey: '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',
  region: 'us-east-1',
});

const sns = new AWS.SNS();

module.exports = async (isIos, token) => {
  const platformApplicationArn = getPlatformApplicationArn(isIos);
  const endpointParams = { PlatformApplicationArn: platformApplicationArn, Token: token };

  return new Promise((resolve, reject) => {
    sns.createPlatformEndpoint(endpointParams, function (errEndpoint, EndPointResult) {
      if (errEndpoint) {
        logger.error(`Error on createPlatformEndpoint: ${errEndpoint}`);
        reject(errEndpoint);
      } else {
        const { EndpointArn } = EndPointResult;
        logger.info(`EndpointArn: ${EndpointArn}`);

        resolve({ EndpointArn });
      }
    });
  });
};

const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const validate = require('./validate');
const { roles } = require('../config/roles');
const authValidation = require('../validations/auth.validation');

const roleValidate = () => (req, res, next) => {
  const { role } = req.body;

  if (!role) {
    const errorMessage = 'Role is required';
    return next(new ApiError(httpStatus.BAD_REQUEST, errorMessage));
  }
  if (!roles.includes(role)) {
    const errorMessage = 'Invalid role';
    return next(new ApiError(httpStatus.BAD_REQUEST, errorMessage));
  }

  return validate(authValidation[`${role}Register`])(req, res, next);
};

module.exports = roleValidate;

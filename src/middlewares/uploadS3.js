const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const config = require('../config/config');

const s3 = new AWS.S3({
  accessKeyId: config.aws.accessKey,
  secretAccessKey: config.aws.secretAccessKey,
});

const uploadS3 = multer({
  storage: multerS3({
    s3,
    acl: 'public-read',
    bucket: 'graduapp-dev-assets',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key(req, file, cb) {
      cb(
        null,
        `${Date.now().toString()}${req.body.user || req.params.userId}${file.mimetype === 'video/mp4' ? '.mp4' : '.jpg'}`
      );
    },
  }),
});

module.exports = uploadS3;

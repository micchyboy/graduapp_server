const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const notificationValidation = require('../../validations/notification.validation');
const notificationController = require('../../controllers/notification.controller');

const router = express.Router();

router.route('/').get(auth(), validate(notificationValidation.getNotifications), notificationController.getNotifications);
router
  .route('/mark-as-seen')
  .post(auth(), validate(notificationValidation.markNotifsAsSeen), notificationController.markNotifsAsSeen);

router
  .route('/store-device-token')
  .post(auth(), validate(notificationValidation.storeDeviceToken), notificationController.storeDeviceToken);

router
  .route('/change-subscription-status')
  .post(auth(), validate(notificationValidation.changeSubscriptionStatus), notificationController.changeSubscriptionStatus);

module.exports = router;

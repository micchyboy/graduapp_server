const express = require('express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerDefinition = require('../../docs/swaggerDef');

const router = express.Router();

const specs = swaggerJsdoc({
  swaggerDefinition,
  apis: ['src/docs/*.yml', 'src/routes/v1/*.js'],
});

const options = {
  explorer: true,
  customCss: '.parameters-col_description [disabled], small:last-child pre.version { display: none };',
};

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(specs, options));

module.exports = router;

const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const uploadS3 = require('../../middlewares/uploadS3');
const feedValidation = require('../../validations/feed.validation');
const feedController = require('../../controllers/feed.controller');

const router = express.Router();

router.route('/').get(auth(), validate(feedValidation.getFeeds), feedController.getFeeds);
router.post(
  '/create-feed',
  auth(),
  uploadS3.array('files[]', 10),
  validate(feedValidation.createFeed),
  feedController.createFeed
);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Feed
 *   description: Feed Creation and Feeds Retrieval with Websocket Connections using Socket.io
 */

/**
 * @swagger
 * path:
 *  /feed/create-feed:
 *    post:
 *      summary: Register as user
 *      tags: [Feed]
 *      consumes:
 *        - multipart/form-data
 *      parameters:
 *        - in: header
 *          name: Content-Type
 *          type: string
 *          required: true
 *          description: multipart/form-data
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - user
 *                - description
 *                - type
 *              properties:
 *                user:
 *                  type: string
 *                description:
 *                  type: string
 *                type:
 *                  type: string
 *                files[]:
 *                  type: object
 *                  properties:
 *                    name:
 *                      type: string
 *                    type:
 *                      type: string
 *                    url:
 *                      type: string
 *              example:
 *               user: 5ebac534954b54139806c112
 *               description: Championship game
 *               type: public
 *               files[0]: {
 *                 name: "championship.png",
 *                 type: "image/png",
 *                 uri: "<device_image_path>/championship.png"
 *               }
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  user:
 *                    $ref: '#/components/schemas/Feed'
 */

/**
 * @swagger
 * path:
 *  Socket.io - "/":
 *    get:
 *      summary: Root path socket connection
 *      tags: [Feed]
 *      responses:
 *        "feeds":
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Feed'
 */

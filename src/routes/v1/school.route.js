const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const schoolValidation = require('../../validations/school.validation');
const schoolController = require('../../controllers/school.controller');

const router = express.Router();

router.get('/', validate(schoolValidation.getSchools), schoolController.getSchools);
router.route('/:schoolId').patch(auth(), validate(schoolValidation.updateSchool), schoolController.updateSchool);
router.post('/invite-schools', validate(schoolValidation.inviteSchools), schoolController.inviteSchools);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: School
 */

/**
 * @swagger
 * paths:
 *  /schools:
 *    get:
 *      summary: Get all schools
 *      tags: [School]
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      type: string
 *              example:
 *                results: [Cambridge University, Harvard University]
 *
 *  /invite-schools:
 *    post:
 *      summary: Invite schools
 *      tags: [School]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - schools
 *                - invitedBy
 *              properties:
 *                schools:
 *                  type: object
 *                  properties:
 *                    email:
 *                      type: string
 *                      format: email
 *                    adminName:
 *                      type: string
 *                    name:
 *                      type: string
 *                invitedBy:
 *                  type: string
 *              example:
 *                schools: [
 *                  {
 *                    email: "rsuplido@gmail.com",
 *                    adminName: "Raymundo B. Suplido",
 *                    name: "De La Salle University"
 *                  }
 *                ]
 *                invitedBy: Jethro Estrada
 *      responses:
 *        "204":
 *          description: No content
 */

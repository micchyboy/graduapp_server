const express = require('express');
const schoolRoute = require('./school.route');
const activityRoute = require('./activity.route');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const feedRoute = require('./feed.route');
const notificationRoute = require('./notification.route');

const router = express.Router();

router.use('/schools', schoolRoute);
router.use('/activities', activityRoute);
router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/docs', docsRoute);
router.use('/feeds', feedRoute);
router.use('/notifications', notificationRoute);

module.exports = router;

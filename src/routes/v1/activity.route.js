const express = require('express');
const validate = require('../../middlewares/validate');
const activityValidation = require('../../validations/activity.validation');
const activityController = require('../../controllers/activity.controller');

const router = express.Router();

router.get('/', validate(activityValidation.getActivities), activityController.getActivities);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Activity
 */

/**
 * @swagger
 * path:
 *  /activities:
 *    get:
 *      summary: Get all activities
 *      tags: [Activity]
 *      parameters:
 *        - in: query
 *          name: school
 *          schema:
 *            type: string
 *          description: School name
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      type: string
 *              example:
 *                results: [Basketball, Chess]
 */

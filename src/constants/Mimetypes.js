export default {
  IMAGE_FORMATS: ['image/jpeg', 'image/jpg', 'image/png'],
  VIDEO_FORMATS: ['video/mp4'],
};

const { Activity } = require('../models');

/**
 * Create a activity
 * @param {Object} activityBody
 * @returns {Promise<Activity>}
 */
const createActivity = async (name) => {
  const existingActivity = await Activity.findOne({ name });
  if (existingActivity) {
    return existingActivity;
  }

  const activity = await Activity.create({ name });
  return activity;
};

const getActivities = async () => {
  // const activities = await Activity.paginate(filter, options);
  // const school = await School.findOne({ name: schoolName });
  // const activities = await Activity.find({ name: { $in: school.activities } });
  const activities = await Activity.find();

  return activities;
};

module.exports = {
  createActivity,
  getActivities,
};

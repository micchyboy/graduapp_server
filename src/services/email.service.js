const nodemailer = require('nodemailer');
const { join, dropRight } = require('lodash');
const config = require('../config/config');
const logger = require('../config/logger');

const transport = nodemailer.createTransport({
  // Yes. SMTP!
  host: 'email-smtp.us-east-1.amazonaws.com', // Amazon email SMTP hostname
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  auth: {
    user: config.aws.smtpUser, // Use from Amazon Credentials
    pass: config.aws.smtpPass, // Use from Amazon Credentials
  },
});
/* istanbul ignore next */
if (config.env !== 'test') {
  transport
    .verify()
    .then(() => logger.info('Connected to email server'))
    .catch(() => logger.warn('Unable to connect to email server. Make sure you have configured the SMTP options in .env'));
}

/**
 * Send an email
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (to, subject, text, html, attachments) => {
  transport.sendMail(
    {
      from: config.email.from,
      to,
      subject,
      html,
      attachments,
    },
    (err, info) => {
      if (info) {
        logger.info(`Success email: ${to}`, info);
      } else if (err) {
        logger.warn(`Error email: ${to}`, err);
      }
    }
  );
};

const sendEmailInvite = async (to, invitee, school, inviter) => {
  const subject = 'Graduapp Invitation';
  const emailVerificationUrl = `${config.host}:${config.port}/redirect?url=${encodeURIComponent(
    `graduapp://school/${to}/${invitee}/${school}`
  )}`;
  const html = `
    <div style="max-width: 375px; font-family: Arial, Helvetica, sans-serif;">
<div style="text-align: center; border: 1px solid #DDD; border-radius: 10px; padding: 30px 20px 30px 20px;">
<img style="width:86px;height:86px;" src="https://graduapp-dev-assets.s3.amazonaws.com/logo.png">
<p style="color: #2A3439; font-weight: bold; font-size: 27px; margin-top: 0px;">Graduapp</p>
<p style="color: #2A3439; text-align: left;">Hello ${join(dropRight(invitee.split(' ')), ' ')},</p>
<p style="color: #2A3439; text-align: left;">${inviter} invited you to join the Graduapp community. The largest social network for graduates.</p>
 
    <a href="${emailVerificationUrl}" style="font-size: 14px; margin-top: 30px; display: block; background-color: #7D52D9; border-radius: 10px; padding: 14px 0 14px 0; color: white; letter-spacing: 1.4px; font-weight: bold; text-align: center; text-decoration: none;">JOIN</a>
</div>
<p style="font-size: 11px; color: #747576; text-align: center;">You received this email to let you know about important changes to your Gaduapp Account and Services.<br>©2020 Graduapp, Chicago, USA</p>
</div>
  `;
  await sendEmail(to, subject, null, html);
};

/**
 * Send email verification email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendEmailVerificationEmail = async (to, token, userName, isDevice) => {
  const subject = 'Confirm Email Address';
  const emailVerificationUrl = `${config.webhost}/email-verification?token=${token}&platform=${isDevice ? 'mobile' : 'web'}`;
  const html = `
    <div style="max-width: 375px; font-family: Arial, Helvetica, sans-serif;">
<div style="text-align: center; border: 1px solid #DDD; border-radius: 10px; padding: 30px 20px 30px 20px;">
<img style="width:86px;height:86px;" src="https://graduapp-dev-assets.s3.amazonaws.com/logo.png">
<p style="color: #2A3439; font-weight: bold; font-size: 27px; margin-top: 0px;">Graduapp</p>
<p style="color: #2A3439; text-align: left;">Hi ${join(dropRight(userName.split(' ')), ' ')},</p>
<p style="color: #2A3439; text-align: left;">Tap the button below to confirm your email address. If you didn’t create an account with Graduapp, you can safely delete this email</p>
 
    <a href="${emailVerificationUrl}" style="font-size: 14px; margin-top: 30px; display: block; background-color: #7D52D9; border-radius: 10px; padding: 14px 0 14px 0; color: white; letter-spacing: 1.4px; font-weight: bold; text-align: center; text-decoration: none;">VERIFY EMAIL</a>
</div>
<p style="font-size: 11px; color: #747576; text-align: center;">You received this email to let you know about important changes to your Gaduapp Account and Services.<br>©2020 Graduapp, Chicago, USA</p>
</div>
  `;
  await sendEmail(to, subject, null, html);
};

const sendParentInvitationEmail = async (to, senderName) => {
  const subject = 'Graduapp Invitation to Register';
  const parentVerificationUrl = `${config.webhost}/register-parent?email=${to}`;

  const html = `
    <div style="max-width: 375px; font-family: Arial, Helvetica, sans-serif;">
<div style="border: 1px solid #DDD; border-radius: 10px; padding: 0 20px 30px 20px;">
<p style="font-weight: bold; font-size: 27px; text-align: center;">Graduapp</p>
<p style="color: #2A3439">Hello,</p>
<p style="color: #2A3439">You are invited by your student, ${senderName}, to join Graduapp community. The largest social network for graduates.</p>
<img style="width:301px;height:167px;" src="https://graduapp-dev-assets.s3.amazonaws.com/gradpic.png">
    <a href="${parentVerificationUrl}" style="font-size: 14px; margin-top: 30px; display: block; background-color: #7D52D9; border-radius: 10px; padding: 14px 0 14px 0; color: white; letter-spacing: 1.4px; font-weight: bold; text-align: center; text-decoration: none;">JOIN NOW</a>
</div>
</div>
  `;
  await sendEmail(to, subject, null, html);
};

/**
 * Send reset password email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, token) => {
  const subject = 'Reset Password';
  // replace this url with the link to the reset password page of your front-end app
  const resetPasswordUrl = `${config.webhost}/reset-password?token=${token}`;
  const html = `
    <div style="max-width: 375px; font-family: Arial, Helvetica, sans-serif;">
<div style="text-align: center; border: 1px solid #DDD; border-radius: 10px; padding: 30px 20px 30px 20px;">
<img style="width:70px;height:70px;" src="https://graduapp-dev-assets.s3.amazonaws.com/password-key.png">
<p style="font-weight: bold; font-size: 23px; margin-top: 20px;">Graduapp</p>
<p style="font-size: 16px; color: #2A3439;">Hello click the button below to<br>reset your password</p>
 
    <a href="${resetPasswordUrl}" style="font-size: 14px; margin-top: 30px; display: block; background-color: #7D52D9; border-radius: 10px; padding: 14px 0 14px 0; color: white; letter-spacing: 1.4px; font-weight: bold; text-align: center; text-decoration: none;">RESET PASSWORD</a>
</div>
<p style="font-size: 11px; color: #747576; text-align: center;">You received this email to let you know about important changes to your Gaduapp Account and Services.<br>©2020 Graduapp, Chicago, USA</p>
</div>
  `;
  await sendEmail(to, subject, null, html);
};

module.exports = {
  transport,
  sendEmail,
  sendEmailInvite,
  sendEmailVerificationEmail,
  sendParentInvitationEmail,
  sendResetPasswordEmail,
};

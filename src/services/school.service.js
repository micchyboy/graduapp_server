const httpStatus = require('http-status');
const { School } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a school
 * @param {ObjectId} userId
 * @param {Object} schoolsBody
 * @returns {Promise<School>}
 */
const createSchool = async (schoolBody) => {
  if (await School.isSchoolTaken(schoolBody.name)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'School exists');
  }

  const school = await School.create({ ...schoolBody });
  return school;
};

const createOrUpdateSchool = async (schoolBody) => {
  const existingSchool = await School.findOne({ name: schoolBody.name });
  if (existingSchool) {
    const schoolActivities = schoolBody.activities.reduce((result, act) => {
      if (!result.some((r) => r.activity.toString() === act.activity)) {
        return result.concat(act);
      }

      return result;
    }, existingSchool.activities);

    Object.assign(existingSchool, { activities: schoolActivities });
    return existingSchool.save();
  }

  const school = await School.create({ ...schoolBody });
  return school;
};

const verifySchool = async (name) => {
  const school = await School.findOne({ name });
  if (!school) {
    throw new ApiError(httpStatus.NOT_FOUND, 'School not found');
  }
};

const getSchools = async () => {
  // const schools = await School.paginate({}, options);
  const schools = await School.find();

  return schools;
};

const updateSchoolById = async (schoolId, updateBody) => {
  const school = await School.findById(schoolId);
  if (!school) {
    throw new ApiError(httpStatus.NOT_FOUND, 'School not found');
  }
  Object.assign(school, updateBody);
  await school.save();

  return school;
};

module.exports = {
  createSchool,
  createOrUpdateSchool,
  verifySchool,
  getSchools,
  updateSchoolById,
};

const { merge } = require('lodash');
const { Notification, UserNotification } = require('../models');
const subscribeTokenToTopic = require('../utils/subscribeTokenToTopic');
const { pushNotificationToTopic } = require('../utils/notificationSender');
const unsubscribeFromTopic = require('../utils/unsubscribeFromTopic');
const deleteEndpoint = require('../utils/deleteEndpoint');
const getEndpointByToken = require('../utils/getEndpointByToken');

/**
 * Create a notification
 * @param {Object} notificationsBody
 * @returns {Promise<Notification>}
 */
const createNotification = async (notificationBody) => {
  const notification = await Notification.create({ ...notificationBody });
  return notification;
};

const getNotifications = async (filter, options) => {
  const sort = { createdBy: -1 };
  if (options.sortBy) {
    const parts = options.sortBy.split(':');
    sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;
  }

  const notifications = await Notification.find(filter)
    .populate({ path: 'user', select: 'name avatarUrl' })
    .limit(options.limit || 60)
    .sort(sort);

  return notifications;
};

const createOrUpdateUserNotif = async (userNotifBody, isIos) => {
  const { deviceTokens = {}, user, school, endpoints } = userNotifBody;
  const existingUserNotif = await UserNotification.findOne({ user });

  if (existingUserNotif) {
    await Promise.all(
      Object.entries(deviceTokens).map(async ([deviceId, deviceToken]) => {
        const oldDeviceToken = existingUserNotif.deviceTokens[deviceId];
        if (oldDeviceToken !== deviceToken) {
          const { EndpointArn: endpointArn } = await getEndpointByToken(isIos, oldDeviceToken);
          const subscriptions = existingUserNotif.endpoints[endpointArn];

          if (subscriptions) {
            const { public: publicArn, school: schoolArn } = subscriptions;
            unsubscribeFromTopic(publicArn);
            unsubscribeFromTopic(schoolArn);
          }

          deleteEndpoint(endpointArn);

          delete existingUserNotif.endpoints[endpointArn];
        }
      })
    );

    Object.assign(existingUserNotif, userNotifBody, {
      deviceTokens: { ...existingUserNotif.deviceTokens, ...deviceTokens },
      endpoints: merge(existingUserNotif.endpoints, endpoints),
    });
    return existingUserNotif.save();
  }

  const userNotif = await UserNotification.create({
    deviceTokens,
    user,
    school,
    endpoints,
  });

  return userNotif;
};

const getUserNotif = async (userId) => {
  return UserNotification.findOne({ user: userId });
};

const storeDeviceToken = async (reqBody) => {
  const { deviceId, isIos, token, user, schoolId } = reqBody;
  const deviceUniqueId = deviceId || user;

  const { SubscriptionArn: subscriptionPublic } = await subscribeTokenToTopic(token, `${schoolId}-public`, isIos);
  const { SubscriptionArn: subscriptionSchool, EndpointArn: endpoint } = await subscribeTokenToTopic(
    token,
    `${schoolId}-school`,
    isIos
  );

  await createOrUpdateUserNotif(
    {
      deviceTokens: { [deviceUniqueId]: token },
      user,
      school: schoolId,
      endpoints: { [endpoint]: { public: subscriptionPublic, school: subscriptionSchool } },
    },
    isIos
  );

  return token;
};

const pushNotifyTokens = async (reqBody) => {
  const { userName, type, school: schoolId } = reqBody;

  if (['public', 'school'].includes(type)) {
    const message = `${userName} has a new post in ${type}`;
    await pushNotificationToTopic(`${schoolId}-${type}`, message);
  }

  return null;
};

const changeSubscriptionStatus = async (userNotifBody) => {
  const { user, type, status, isIos } = userNotifBody;
  const userNotif = await UserNotification.findOne({ user });

  if (userNotif && userNotif.deviceTokens) {
    if (!status) {
      Object.values(userNotif.endpoints).forEach((subscription) => {
        const subscriptionArn = subscription[type];
        unsubscribeFromTopic(subscriptionArn);
      });
    } else {
      const endpoints = {};
      await Promise.all(
        Object.values(userNotif.deviceTokens).map(async (token) => {
          const { SubscriptionArn: subscriptionArn, EndpointArn: endpointArn } = await subscribeTokenToTopic(
            token,
            `${userNotif.school}-${type}`,
            isIos
          );
          endpoints[endpointArn] = { [type]: subscriptionArn };
        })
      );

      createOrUpdateUserNotif(
        {
          user,
          endpoints,
        },
        isIos
      );
    }
  }
};

module.exports = {
  createNotification,
  getNotifications,
  createOrUpdateUserNotif,
  getUserNotif,
  storeDeviceToken,
  pushNotifyTokens,
  changeSubscriptionStatus,
};

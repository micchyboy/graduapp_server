module.exports.activityService = require('./activity.service');
module.exports.schoolService = require('./school.service');
module.exports.authService = require('./auth.service');
module.exports.emailService = require('./email.service');
module.exports.tokenService = require('./token.service');
module.exports.userService = require('./user.service');
module.exports.feedService = require('./feed.service');
module.exports.notificationService = require('./notification.service');

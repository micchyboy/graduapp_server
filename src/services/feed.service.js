const httpStatus = require('http-status');
const { Feed } = require('../models');
const ApiError = require('../utils/ApiError');

const populateFeed = async (feed) => {
  const populatedFeed = await feed.populate('user', 'name avatarUrl').execPopulate();

  return populatedFeed;
};
/**
 * Create a feed
 * @param {Object} feedBody
 * @returns {Promise<Feed>}
 */
const createFeed = async (feedBody, feedFiles) => {
  const feed = await Feed.create({ ...feedBody, files: feedFiles });
  return populateFeed(feed);
};

const getFeedById = async (id) => {
  const feed = await Feed.findById(id);
  return populateFeed(feed);
};

const updateFeedById = async (feedId, updateBody) => {
  const feed = await getFeedById(feedId);
  if (!feed) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Feed not found');
  }

  Object.assign(feed, updateBody);
  await feed.save();

  return populateFeed(feed);
};

const queryFeeds = async (filter, options) => {
  const query = {};
  const optionsObj = { ...options };
  optionsObj.populate = { path: 'user', select: 'name avatarUrl' };

  if (filter.categories) {
    query.$or = [];
    const categoriesArr = filter.categories.split(',');

    categoriesArr.forEach((category) => {
      query.$or.push({ 'categories.name': category });
      query.$or.push({ 'categories.subCategories': category });
    });
  }

  const feeds = await Feed.paginate(query, optionsObj);
  return feeds;
};

module.exports = {
  createFeed,
  getFeedById,
  updateFeedById,
  queryFeeds,
};

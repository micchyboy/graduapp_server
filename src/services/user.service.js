const httpStatus = require('http-status');
const { union, reduce, find, get } = require('lodash');
const { User } = require('../models');
const activityService = require('./activity.service');
const schoolService = require('./school.service');
const ApiError = require('../utils/ApiError');

const populateUser = async (user) => {
  const populatedUser = await user
    .populate({
      path: 'school',
      populate: {
        path: 'activities.activity',
        model: 'Tblt_activity',
      },
    })
    .populate('extraCurricularInSchool', 'name')
    .populate('extraCurricularOutSchool', 'name')
    .execPopulate();

  return populatedUser;
};

const createUserActivities = async (userBody) => {
  const activityKeys = ['activities', 'extraCurricularInSchool', 'extraCurricularOutSchool'];
  const userActivities = union(...activityKeys.map((key) => get(userBody, key)));
  const activities = await Promise.all(
    userActivities.map(async (act) => {
      const activity = await activityService.createActivity(act);

      return activity;
    })
  );

  const activitiesObj = reduce(
    activityKeys,
    function (result, key) {
      const updatedActivitiesObj = { ...result };
      const activityArr = get(userBody, key);
      if (activityArr) {
        updatedActivitiesObj[key] = activityArr.map((act) => find(activities, ['name', act]).id);
      }
      return updatedActivitiesObj;
    },
    {}
  );

  return activitiesObj;
};

const createOrUpdateUserSchool = async (schoolBody) => {
  const schoolObj = { ...schoolBody };

  if (schoolBody.activities) {
    schoolObj.activities = schoolBody.activities.map((actId) => ({ activity: actId, isVerified: true }));
  } else {
    const studentActivities = union(schoolBody.extraCurricularInSchool, schoolBody.extraCurricularOutSchool);

    schoolObj.activities = studentActivities.map((actId) => ({ activity: actId, isVerified: false }));
  }

  const school = await schoolService.createOrUpdateSchool(schoolObj);

  return school.id;
};

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }

  const user = await User.create(userBody);

  return populateUser(user);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'No user found with this email');
  }

  return populateUser(user);
};

const getStudentUserByParentEmail = async (email) => {
  const user = await User.findOne({ parentEmail: email });

  if (!user) {
    throw new ApiError(
      httpStatus.NOT_FOUND,
      'You must have your child register first and\nfill up "Parent Email" field with your email'
    );
  }

  return populateUser(user);
};

const flattenUser = (populatedUser) => {
  const user = populatedUser.toJSON();

  if (user.school && user.school.activities) {
    Object.assign(user, {
      school: {
        ...user.school,
        activities: user.school.activities.reduce((result, act) => {
          const activity = result.find((r) => r.category === 'Sports') || { category: 'Sports', subCategories: [] };

          activity.subCategories.push(act.activity.name);

          return result.filter((r) => r.category !== 'Sports').concat(activity);
        }, []),
      },
    });
  }
  if (user.extraCurricularInSchool) {
    Object.assign(user, { extraCurricularInSchool: user.extraCurricularInSchool.map((act) => act.name) });
  }
  if (user.extraCurricularOutSchool) {
    Object.assign(user, { extraCurricularOutSchool: user.extraCurricularOutSchool.map((act) => act.name) });
  }

  return user;
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  Object.assign(user, updateBody);
  await user.save();

  return populateUser(user);
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};

module.exports = {
  createOrUpdateUserSchool,
  createUserActivities,
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  getStudentUserByParentEmail,
  updateUserById,
  deleteUserById,
  flattenUser,
};

const httpStatus = require('http-status');
const { pick } = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { userService, notificationService } = require('../services');

const getNotifications = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.query.user);

  const filter = pick(req.query, ['type']);
  filter.school = user.school;
  filter.user = { $ne: user.id };
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const notifications = await notificationService.getNotifications(filter, options);
  const userNotif = await notificationService.getUserNotif(user.id);
  const results = {
    results: notifications.map((notif) => {
      const notifObj = notif.toJSON();
      const isSeen = !userNotif ? false : userNotif.notifications.includes(notifObj.id);
      return { ...notifObj, isSeen };
    }),
  };
  res.send(results);
});

const markNotifsAsSeen = catchAsync(async (req, res) => {
  await notificationService.createOrUpdateUserNotif(req.body);
  res.status(httpStatus.NO_CONTENT).send();
});

const storeDeviceToken = catchAsync(async (req, res) => {
  await notificationService.storeDeviceToken(req.body);
  res.status(httpStatus.NO_CONTENT).send();
});

const changeSubscriptionStatus = catchAsync(async (req, res) => {
  await notificationService.changeSubscriptionStatus(req.body);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getNotifications,
  markNotifsAsSeen,
  storeDeviceToken,
  changeSubscriptionStatus,
};

const httpStatus = require('http-status');
const { get, pick, dropRight } = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { feedService, userService, notificationService } = require('../services');
const generateThumbnailS3 = require('../utils/generateThumbnailS3');
const ApiError = require('../utils/ApiError');

const createFeed = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.body.user);

  if (!user.school) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not allowed to create feed');
  }

  const files = req.files.map((file, index) => {
    const fileLocation = dropRight(file.location.split('.'));
    const fileRoute = fileLocation.join('.');

    return {
      name: file.originalname,
      type: file.mimetype,
      thumbnail: `${fileRoute}-thumbnail.jpg`,
      url: file.location,
      caption: get(req.body, `captions[${index}]`, ''),
    };
  });

  const feed = await feedService.createFeed({ ...req.body, school: user.school, createdBy: user.name }, files);

  if (feed.type !== 'private') {
    await notificationService.createNotification({
      user: user.id,
      type: feed.type,
      school: user.school,
      feed: feed.id,
    });

    await notificationService.pushNotifyTokens({
      userName: user.name,
      type: feed.type,
      school: user.school,
    });
  }

  res.status(httpStatus.CREATED).send(feed);

  const filesWithThumbnails = await Promise.all(
    files.map(async (file) => {
      const thumbnail = await generateThumbnailS3(file.url, file.thumbnail, file.type);
      return { ...file, thumbnail };
    })
  );

  feedService.updateFeedById(feed.id, { files: filesWithThumbnails });
});

const getFeeds = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.query.user);

  const filter = pick(req.query, ['type', 'categories']);
  filter.school = user.school;
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await feedService.queryFeeds(filter, options);
  res.send(result);
});

module.exports = {
  createFeed,
  getFeeds,
};

const httpStatus = require('http-status');
const { pick } = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { schoolService, emailService } = require('../services');

const getSchools = catchAsync(async (req, res) => {
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const schools = await schoolService.getSchools(options);

  const results = { results: schools.map((school) => school.name) };
  res.send(results);
});

const inviteSchools = catchAsync(async (req, res) => {
  const { schools, invitedBy } = req.body;
  schools.forEach((school) => {
    emailService.sendEmailInvite(school.email, school.adminName, school.name, invitedBy);
  });

  res.status(httpStatus.NO_CONTENT).send();
});

const updateSchool = catchAsync(async (req, res) => {
  const school = await schoolService.updateSchoolById(req.params.schoolId, req.body);
  res.send(school);
});

module.exports = {
  getSchools,
  inviteSchools,
  updateSchool,
};

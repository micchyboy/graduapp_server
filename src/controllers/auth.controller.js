const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService, emailService, schoolService } = require('../services');
const Const = require('../constants');

const register = catchAsync(async (req, res) => {
  if (req.body.role === Const.ROLES.STUDENT) {
    await schoolService.verifySchool(req.body.school);
  }
  const activitiesObj = await userService.createUserActivities(req.body);

  let schoolId;
  let userBody = { ...req.body };

  if (req.body.role !== Const.ROLES.PARENT) {
    schoolId = await userService.createOrUpdateUserSchool({ ...req.body, name: req.body.school, ...activitiesObj });
    userBody = { ...userBody, school: schoolId, ...activitiesObj };
  }

  const user = await userService.createUser(userBody);

  res.status(httpStatus.CREATED).send({ user: userService.flattenUser(user) });

  const emailVerificationToken = await tokenService.generateEmailVerificationToken(user.id);
  const isDevice = req.useragent.isMobile || req.useragent.isTablet;
  emailService.sendEmailVerificationEmail(user.email, emailVerificationToken, user.name, isDevice);
  if (user.parentEmail) {
    emailService.sendParentInvitationEmail(user.parentEmail, user.name);
  }
});

const emailVerification = catchAsync(async (req, res) => {
  await authService.emailVerification(req.query.token);
  res.send('Verification Success');
});

const resendVerification = catchAsync(async (req, res) => {
  const user = await userService.getUserByEmail(req.body.email);
  const emailVerificationToken = await tokenService.generateEmailVerificationToken(user.id);
  const isDevice = req.useragent.isMobile || req.useragent.isTablet;
  emailService.sendEmailVerificationEmail(req.body.email, emailVerificationToken, user.name, isDevice);
  res.status(httpStatus.NO_CONTENT).send();
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  let user = await authService.loginUserWithEmailAndPassword(email, password);

  const tokens = await tokenService.generateAuthTokens(user);

  if (user.role === Const.ROLES.PARENT) {
    const student = await userService.getStudentUserByParentEmail(user.email);
    user = { ...userService.flattenUser(student), role: Const.ROLES.PARENT };
  } else {
    user = userService.flattenUser(user);
  }

  res.send({ user, tokens });
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const user = await userService.getUserByEmail(req.body.email);
  const resetPasswordToken = await tokenService.generateResetPasswordToken(user.id);
  await emailService.sendResetPasswordEmail(req.body.email, resetPasswordToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPassword = catchAsync(async (req, res) => {
  await authService.resetPassword(req.query.token, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  register,
  resendVerification,
  emailVerification,
  login,
  refreshTokens,
  forgotPassword,
  resetPassword,
};

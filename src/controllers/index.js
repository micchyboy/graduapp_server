module.exports.activityController = require('./activity.controller');
module.exports.schoolController = require('./school.controller');
module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.feedController = require('./feed.controller');
module.exports.notificationController = require('./notification.controller');

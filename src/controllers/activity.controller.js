const { pick } = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { activityService } = require('../services');

const getActivities = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['isVerified', 'school']);
  // const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const { isVerified, school } = filter;
  const activities = await activityService.getActivities(isVerified, school);

  const results = { results: activities.map((activity) => activity.name) };
  res.send(results);
});

module.exports = {
  getActivities,
};
